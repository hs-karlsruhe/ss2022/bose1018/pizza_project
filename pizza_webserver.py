from flask import Flask, request, jsonify, abort

TOPPINGS = ["Mozzarella", "Cheddar", "Mushrooms", "Tomatoes", "Salami", "Prosciutto", "Spinach"]
ORDERS = []

app = Flask(__name__)


def check_topping(topping):
    """Check if the topping is available.

    :param topping: The topping on the pizza
    :return: If the topping is available or not
    """
    if topping in TOPPINGS:
        return True
    return False


@app.route('/')
def hello_user():
    """Return a simple greating

    :return: Simple greating
    """
    return '{"message": "Hungry? Order a pizza with our Pizza Delivery API!"}', 200

@app.route('/order')
def order():
    """Order a pizza with at least 1 and up to 3 toppings. The first topping (parameter topping1) must always be given.
    Toppings 2 and 3 are optional. If topping2 is not given, topping3 can still be given as the second topping.

    :param topping1: first topping on the pizza
    :param topping2: second topping on the pizza
    :param topping3: third topping on the pizza
    :return: List of toppings ordered on the pizza
    """

    topping1 = request.args.get('topping1')
    topping2 = request.args.get('topping2')
    topping3 = request.args.get('topping3')
    if topping1 is None or topping1 =="":
        abort(406, description="Please choose at least the first topping.")
    topping_array = [topping1, topping2, topping3]
    topping_array_clean = list(filter(None, topping_array))
    i = 0
    while i < len(topping_array_clean):
        if not check_topping(topping_array_clean[i]):
            abort(404,
                  description="At least one of the toppings is not available. The following toppings are available: Mozzarella, Cheddar, Mushrooms, Tomatoes, Salami, Prosciutto, Spinach")
        i = i+1
    order_dic = {"toppings": topping_array_clean}
    ORDERS.append(order_dic)
    return jsonify(toppings=topping_array_clean), 200

<<<<<<< HEAD
=======
@app.route('/order')
def order():
    """Return the Toppings"""
    topping1 = request.args.get('topping1')
    topping2 = request.args.get('topping2')
    topping3 = request.args.get('topping3')
    topping_array = [topping1, topping2, topping3]
    if topping1 is None:
        abort(406, description="Please choose at least the first topping.")
    topping_array_clean = list(filter(None, topping_array))
    i = 0
    while i < len(topping_array_clean):
        if check_topping(topping_array_clean[i]):
            abort(404, description="At least one of the toppings is not available. The following toppings are available: Mozzarella, Cheddar, Mushrooms, Tomatoes, Salami, Prosciutto, Spinach")
        i = i + 1
    order_dic = {"toppings": topping_array_clean}
    ORDERS.append(order_dic)
    return jsonify(toppings=topping_array_clean), 200

@app.route('/orders')
def orders():
    return jsonify(list(ORDERS)), 200

@app.route('/deleteOrders')
def deleteOrders():
    ORDERS.clear()
    return jsonify(message="The orders were deleted successfully."), 200


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0", port=8080)
>>>>>>> 698500a9aeb4a24d381988ee316892c076ee5392

@app.route('/orders')
def orders():
    """Get order history.

      return: A list of all orders
      """
    return jsonify(list(ORDERS)), 200

@app.route('/deleteOrders')
def deleteOrders():
    """Delete all orders.

    :return: Confirmation of successful deletion process
    """
    ORDERS.clear()
    return jsonify(message="The orders were deleted successfully."), 200

if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0", port=8090)
